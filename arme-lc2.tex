\documentclass[10pt,compress,red]{beamer}
%\usepackage{beamerarticle}
\usepackage{pgf, soul }  %note: package 'soul' is used for highlighting
\usepackage{epstopdf}
\usepackage{dtklogos,verbatim,booktabs}
\setcounter{tocdepth}{2}
\listfiles  %check log file for file dependencies
\usepackage{graphicx, url, amsmath, multicol, fixltx2e}
\usepackage[font=small,format=plain,labelfont=bf,justification=raggedright,singlelinecheck=true,skip=0pt]{caption}
\usetheme{Berlin}  %PaloAlto or Copenhagen for short presentations, long:  JuanLesPins, or Antibes , or CambridgeUS or Berlin
\setbeamertemplate{caption}[numbered]
\usecolortheme{beaver}
 \hypersetup{
    	 pdfnewwindow=true,      % links in new window
   	 colorlinks=true,       % false: boxed links; true: colored links
  	  linkcolor=red,          % color of internal links
  	  citecolor=green,        % color of links to bibliography
  	  filecolor=magenta,      % color of file links
  	  urlcolor=red 
    }
 \AtBeginSection{
 \begin{frame}[shrink]
 \frametitle{Contents}
 \begin{multicols}{2}
 \tableofcontents[currentsection]
 \end{multicols}
 \end{frame}
 }
 

\title{A Concise Course in \LaTeX{}\\ Part 2: Advanced Document Elements}
\author{Hector Rufrancos}
\institute{University of Sussex}
\date{July 2014}


\begin{document}

\frame{\titlepage}

%\section{Table of Contents}
\begin{frame}[shrink]{Table of Contents}
    \frametitle{Contents}
		\begin{multicols}{2}
		\tableofcontents[currentsection]
		\end{multicols}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
	\subsection{Outline}
			\begin{frame}[shrink]{Outline}
			Last time we learnt:
				\begin{itemize}
					\item Basics of the document structure
					\item Quotes, symbols, special characters and sectioning
				\end{itemize}
			This session we will:
				\begin{itemize}
					\item Article class \& Report class
					\item Packages
						\begin{itemize}
							\item graphicx
							\item hyperref
							\item endnotes
						\end{itemize}
					\item Tables
					\item Table of Contents, Lists, \texttt{\textbackslash verb}
				\end{itemize}
			\end{frame}
			
	\subsection{Recap}
		\begin{frame}[shrink, containsverbatim]{A Simple \LaTeX{} file}
			\begin{columns}
			\column{.5\textwidth}			
			\begin{block}{Example}
				{\tiny				
				\begin{verbatim}
				\documentclass[11pt,a4paper]{article}
				% This part is the preamble
				% You can load packages by uncommenting the line below:
				%\usepackage[options]{somepackagehere}				
				\title{My Awesome Paper}		
				\author{Hector Rufrancos\\ University of Sussex}
				\date{\today}
				\begin{document}
				\maketitle
					\begin{abstract}
						This is the abstract to my really clever paper.
					\end{abstract}
				\section{Introduction}
					This is the first section. Cicero \cite{cicero} said 
					``\ldots which of us ever undertakes laborious physical
					exercise, except to obtain some advantage from it?'' 
					% a longer quote should use the quote environment.
				\subsection{Some stuff}
					This is my first subsection.
				\section{Body}
					This is the main meat of the article.
				\section{Conclusions}
					This article was awesome.
				\begin{thebibliography}{10}
					\bibitem{cicero} Cicero (84BCE). de Finibus Bonorum et 
					Malorum. Section 1.10.33
				\end{thebibliography}
				\end{document}
				\end{verbatim}
				}
			\end{block}
			\column{.5\textwidth}
			\includegraphics[scale=.5]{recap}
			\end{columns}
			\end{frame}
\section{Article and Report class}
	\subsection{Article and Report class}
		\begin{frame}{Article and Report class}
			Last session we talked about these very briefly. What are the differences?\\
			The basic syntax for selecting either is \texttt{\textbackslash documentclass[\emph{opt}]\{article/report\}}
			\begin{itemize}
				\item Reports start with a separate title page. Using article can be achieved with titlepage option
				\item Report class can take chapters---So it may be good for something like a DPhil thesis \ldots
				\item Not really any other differences
			\end{itemize}
		\end{frame}
	\subsection{Referencing}
	\begin{frame}[shrink, containsverbatim]{Labels and References}
				\begin{description}
			\item[\textbackslash label\{marker\}] - you give the object you want to reference a marker, you can see it 						like a name.\\
			\item[\textbackslash ref\{marker\}] - you can reference the object you have marked before. This prints the 						number that was assigned to the object.\\
			\item[\textbackslash pageref\{marker\}] -  It will print the \# of the page where the object is. 
			\end{description}
				\begin{block}{Typesetting}
					\begin{verbatim}
					\section{Greetings}
					\label{greetings}
					      Hello World!
					
					\section{Referencing}
					       I greeted in section \ref{greetings}.
					\end{verbatim}
				\end{block}
			\end{frame}
	\subsection{Co-authors and multiple affiliations}
		\begin{frame}[shrink,containsverbatim]{Multiple Affiliations and Co-Authors}	
			\begin{itemize}
				\item[Q:] How do you show author affiliations in a \LaTeX{} article?
				\item[A:] By using the \texttt{authblk} package.
			\end{itemize}
			\begin{columns}
			\column{.5\textwidth}
			\begin{block}{Example}
				{\scriptsize				
				\begin{verbatim}
				\documentclass{article}
				\usepackage{authblk}
				\author[a]{J. T. Ripper}
				\affil[a]{Bethlem Royal \\
				Psychiatric Hospital}
				\author[b]{Bugs Bunny}
				\affil[b]{Looney Tunes}
				\author[b c]{Daffy Duck}
				\affil[c]{Warner Brothers}
				\title{More than one Author\\
				 with different Affiliations}
				\date{}
				\begin{document}								
				\maketitle
				\end{document}
				\end{verbatim}
				}
			\end{block}
			\column{.5\textwidth}
			\includegraphics[width=\textwidth]{authblk}
			\end{columns}
		\end{frame}
		
	\subsection{\texttt{\textbackslash include\{\}}}
		\begin{frame}[shrink,containsverbatim]{Long Documents}
		For smaller documents such as a brief article you will find it easy to use a simple \LaTeX file. But what about larger documents?\\
		We can use the \texttt{\textbackslash include\{\}} command, this allows you include other \texttt{.tex} files\\ 
		\begin{columns}
		\column{.5\textwidth}				
			\begin{block}{Example of the master file}
				\begin{verbatim}
				\documentclass[...]{article}
				<.....>
				\begin{document}
					    \include{firstfile}
					    \include{secondfile}
				<.....>
					    \include{lastfile}
					\end{document}
				\end{verbatim}	
			\end{block}
		\column{.5\textwidth}
			\begin{block}{Example of including file}
				\begin{verbatim}
				\begin{abstract}
				<.....>
				\end{abstract}
				\section{Section the First}
				<.....>
				\end{verbatim}
			\end{block}
		\end{columns}
		\end{frame}
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Packages}
	\subsection{Packages}
		\begin{frame}[shrink,containsverbatim]{Packages: A recap}
		Packages are user-created scripts that extend the functionality of \LaTeX.\\
		Syntax for packages is really quite simple \texttt{\textbackslash usepackage[\emph{opt}]\{\emph{packagename}\}}\\
		Using more than one package is also simple:
			\begin{columns}
				\column{.5\textwidth}
					\begin{block}{Example 1}
						\begin{verbatim}
						\documentclass[...]{article}
						\usepackage{package1,package2}
						<......>
						\end{verbatim}
					\end{block}			
				\column{.5\textwidth}
					\begin{block}{Example 2}
						\begin{verbatim}
						\documentclass[...]{article}
						\usepackage{package1,package2}
						\usepackage[opts]{package3}
						<......>
						\end{verbatim}
					\end{block}
			\end{columns}
		\end{frame}
		
	\subsection{hyperref}
			\begin{frame}[containsverbatim]{\texttt{hyperref} package}
			The hyperref package is useful and massive. We will only cover a few of its features.\\
				\begin{block}{Setup}
				\scriptsize{				
				\begin{verbatim}
				\usepackage{hyperref}
				\hypersetup{
	backref=true,		  % link backreferences (i.e. citations to their list)
    bookmarks=true,         % show bookmarks bar?
    unicode=false,          % non-Latin characters in Acrobat’s bookmarks
    pdftoolbar=true,        % show Acrobat’s toolbar?
    pdfmenubar=true,        % show Acrobat’s menu?
    pdffitwindow=false,     % window fit to page when opened
    pdfstartview={FitH},    % fits the width of the page to the window
    pdftitle={},            % title
    pdfauthor={},           % author
    pdfsubject={},          % subject of the document
    pdfkeywords={}{},       % list of keywords
    pdfnewwindow=true,      % links in new window
    colorlinks=true,        % false: boxed links; true: colored links
    linkcolor=blue,         % colour of internal links
    citecolor=blue,         % colour of links to bibliography
    urlcolor=blue,
    }
				\end{verbatim}
				}
				\normalsize
				\end{block}
			\end{frame}
			
			\begin{frame}[shrink,containsverbatim,label=hyper]{\texttt{hyperref} package}			
			Of course you can always skip a few of the options and keep the ones you really care about.
			\begin{block}{Example}
			\begin{verbatim}
			\usepackage{hyperref}
			\end{verbatim}
			\end{block}
			This only links the cross references and allows for urls to be included through \texttt{\textbackslash url\{\emph{url}\}}\\
			or you can link \texttt{\textbackslash href\{\emph{url}\}\{\emph{text}\}}\\
			\begin{block}{\textbackslash href example}
			\href{mailto:nobody@sussex.ac.uk}{nobody@sussex.ac.uk}\\
			\verb!\href{mailto:nobody@sussex.ac.uk}{nobody@sussex.ac.uk}!
			\end{block}
			\begin{block}{Hyperlinked references}
			Check out this: \hyperlink{cats}{Figure 1} \\
			Code: \verb!\hyperref[cat]{Figure \ref*{cat}}!
			\end{block}
			\end{frame}
			
		\subsection{\texttt{endnotes}}
			\begin{frame}[containsverbatim]{Endnotes in your document}
			Last session we saw the command \texttt{\textbackslash footnote\{\emph{text}\}}\\
			Today, I introduce you to the \texttt{\textbackslash endnote\{\emph{text}\}} command.\\
			Unsurprisingly, it as a package. Notice a theme in this section?\\
			Setup is simple \verb!\usepackage{endnotes}!\\
			\begin{block}{Footnotes to endnotes}
			Say that you wish to do this after typing out the whole article\\
			All you need to do is add the following to your preamble:\\
			\begin{verbatim}
			\let\footnote=\endnote
			\end{verbatim}
			And compile again
			\end{block}
			\end{frame}
		\subsection{\texttt{lastpage}}
			\begin{frame}[shrink, containsverbatim]{Page Numbering}
			Is automatic in \LaTeX. But if you want:
			\begin{block}{Page N of M}
				Need to \verb!\usepackage{fancyhdr,lastpage}!  \\
				And include the following in the preamble: \verb+\cfoot{Page \thepage\ of \pageref*{LastPage}}+
				\verb!\pagestyle{fancy}!\\
				\verb!\cfoot{Page \thepage\ of \pageref*{LastPage}}! \\
				And finally after \verb!\maketitle! you will need to add: \\
				\verb!\thispagestyle{fancy}! \\
				And yo can now compile twice to get your footers. Note that the asterisk is only needed if the \verb!hyperref! package is in use.
			\end{block}
			\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Graphics}
		\subsection{\texttt{graphicx}}
			\begin{frame}[shrink,containsverbatim]{Including graphics in your documents}
			Graphics in \LaTeX{} are considered floats, that is elements that cannot be split across pages.
				\begin{itemize}
				\item To include them in our files we must use the graphicx package.
				\end{itemize}
			As we are using \texttt{pdflatex} we can use the following formats:
				\begin{itemize}
				\item png, .jpg, .gif, .pdf, .tiff
				\end{itemize}
			To load the graphics environment we type the following in the preamble:\\
			\verb!\usepackage{graphicx,xcolor}!\\
			To include an image in our file we must type:\\
			\verb!\includegraphics[key=value,...]{file}!\\
			Where key can be any one of the following:
				\begin{center}
				\begin{tabular}{l c}
				\texttt{width} & scale graphic to the specified width\\
				\texttt{height} & scale graphic to the specified height\\
				\texttt{angle} & rotate graphic counterclockwise\\
				\texttt{scale} & scale graphic	\\
				\end{tabular}
				\end{center}
				\smallskip
			The values may be specified for \texttt{width} \& \texttt{height} in cm, in, and pt.\\
			Scale is given in a proportion of the image.
			\end{frame}
		\subsection{Float Properties}
			\begin{frame}[shrink, containsverbatim]{Floats}
			A word of warning. Floats can be quite annoying.\\
			We need to declare them correctly to avoid frustration\\
			\verb!\begin{figure}![\emph{placement specifier}]\\
			Placement specifiers are:
				\begin{center}
					\begin{tabular}{l l}
					Spec & Permission to place float...\\
					\hline
					h & \emph{here} at the very place in the text where it oc-\\
					&curred. This is useful mainly for small float\\
					t & at the \emph{top} of a page\\
					b & at the \emph{bottom} of a page\\
					p & on a special \emph{page} containing only floats.\\
					! & ignores all parameters and tries to place the float.\\
					& Note that this may prevent float from appearing\\
					\end{tabular}
				\end{center}
			\end{frame}
		\subsection{Example}		
			\begin{frame}[containsverbatim,label=cats]{Example}
				\begin{columns}
				\column{.5\textwidth}				
				\begin{figure}
					\centering
					\label{cat}
					\caption{I'm Rich!}
					\includegraphics[width=\textwidth]{cat}
				\end{figure}
				\column{.5\textwidth}			
				\begin{block}{Code}
				\begin{verbatim}
					\begin{figure}[!h]
					\centering
					\label{cat}
					\caption{I'm Rich!}
					\includegraphics[scale=.5]{cat}
					\end{figure}
				\end{verbatim}
				\end{block}
				\end{columns}
				\smallskip
				As can be seen in \hyperref[cat]{Figure 1}, I'm rich.
				To refer to something you have labeled use \texttt{\textbackslash\{\emph{name}\}} \\ \vfill
				\hyperlink{hyper}{\beamergotobutton{We'll come back to this later}}
			\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%		
\section{Tables}
	\subsection{Basics}
	\begin{frame}[shrink,containsverbatim]{Tables}
			\begin{table}
				\caption{A hideous but useful table}
				\label{table}
				\begin{tabular}{|l|p{4cm}|}
				\hline
				Spec & What it does\\
				\hline
				\hline
				\verb!|!    & adds a vertical line\\
				\hline
				r    & right justifies text in column\\
				\hline
				l    & left justifies text in column\\
				\hline
				c    & centre justifies text in column\\
				\hline
				\&   & Separates entries between columns\\
				\hline
				p\{\emph{width}\} & Special column which will wrap text around by width\\
				\hline
				\verb!\hline! & horizontal line\\
				\hline
				\verb!\cline{i-j}! & Partial horizontal line between columns i and j\\
				\hline
				\end{tabular}					
				\end{table}
	\end{frame}
	\begin{frame}[containsverbatim]{Tables}
	Tables are a type of float so the same rule as graphics apply. They cannot be (easily) split into multiple pages. 
	\begin{scriptsize}
			\begin{block}{Code}			
				\begin{verbatim}
				\begin{table}
				\caption{A hideous but informative table}
				\label{badtable}
				\begin{tabular}{|l|p{4cm}|} \hline
				Spec & What it does\\
				\hline \hline
				|    & adds a vertical line\\ \hline
				r    & right justifies text in column\\ \hline
				l    & left justifies text in column\\ \hline
				c    & centre justifies text in column\\ \hline
				\&   & Separates entries between columns\\ \hline
				p{width} & Special column which will wrap text around by width\\ \hline
				\hline & horizontal line\\ \hline
				\cline{i-j} & Partial horizontal line between columns i and j\\ \hline
				\end{tabular}					
				\end{table}
				\end{verbatim}
			\end{block}
			\end{scriptsize}
	\end{frame}
	\subsection{\texttt{booktabs}}
	\begin{frame}{\texttt{booktabs} Tables}
	That table was horrible! If we wish to have publication type tables we should use the package \texttt{booktabs}.
	Here's an example:\bigskip
		\begin{table}
		\label{goodtable}
		\caption{Some OLS Estimates}
			\begin{tabular}{l c c}
			%\multicolumn{3}{l}{\textbf{Table \ref{goodtable}} Some OLS Estimates}\\			
			\toprule
			& Model 1 & Model 2\\
			\midrule
			Constant & 0.52*** & 0.37***\\
			& (.03)& (.07)\\
			Age & 0.04*** & 0.03***\\
			& (.001) & (.003)\\
			\bottomrule \\
			\multicolumn{3}{l}{Notes: *** denotes significance at 1 per cent}			
			\end{tabular}
		\end{table}
	\end{frame}
	
	\begin{frame}[containsverbatim]{\texttt{booktabs} Tables}
	%And now for the code:
		\begin{block}{Source}
		\scriptsize{
			\begin{verbatim}
			\begin{table}
			\label{goodtable}
			\caption{Some OLS Estimates}			
			\begin{tabular}{l c c}
			\toprule
			& Model 1 & Model 2\\
			\midrule
			Constant & 0.52*** & 0.37***\\
			& (.03) & (.07)\\
			Age & 0.04*** & 0.03***\\
			& (.001) & (.003)\\
			\bottomrule \\
			\multicolumn{3}{l}{Notes: *** denotes significance at 1 per cent}			
			\end{tabular}
			\end{table}
			\end{verbatim}
			}				
		\end{block}
	\scriptsize{Note the \texttt{\textbackslash multicolumn\{\#\}\{\emph{pos}\}\{\emph{text}\}} command. You can also use this in regular \texttt{\textbackslash tabular}\\
	Also, note the name of the table by using the \texttt{\textbackslash ref\{\}} it allows movement of the table anywhere in the text and still have the right title.}
	\end{frame}
	\subsection{Breaking the Rules}
	\begin{frame}[shrink,containsverbatim]{Breaking the \LaTeX{} table rules}
	Now, there may be times when you want to make ridiculously long tables. Or, even sideways tables.\\
	Packages to the rescue. However, you should only do this if absolutely necessary.\\
	\begin{block}{Code}
	\begin{verbatim}	
	\usepackage{booktabs,longtables,landscape}
	\setlongtables
	<.....>
	\begin{landscape}
	\begin{longtable}{...}
	<... Some ridiculously long table..>
	\end{longtable}
	\end{landscape}
	<.....>
	\end{verbatim}
	\end{block}
	\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Other Elements}
	\begin{frame}{Back to the article and report class again}
	Now that we have learnt to use \texttt{graphicx} and \texttt{tabular} we can piece it all together and include a TOC as well as table of figures and table of tables. You'll see it's really simple.
	\end{frame}
	\subsection{TOC}
			\begin{frame}[containsverbatim]{Table of Contents}

				\verb!\tableofcontents! where you want to place the table. And run it twice.\\

					\hspace{.5in} \verb!\listoffigures and \listoftables! work the same way.
				%\pause
				\begin{block}{Example}
					\begin{figure}
					\includegraphics[scale=.7]{toc3}
					\end{figure}
				\end{block}
			\end{frame}
	\subsection{Lists}		
			\begin{frame}[shrink, containsverbatim]{Lists}
				\setlength{\columnseprule}{1pt}
				%working here
				\begin{block}{\texttt{\textbackslash itemize}}
				\begin{columns}
				\column{.5\linewidth}
				{\scriptsize
					\begin{verbatim}
					          \begin{itemize}
  					        \item The first item
 					          \item The second item
 					          \item The third etc \ldots
					          \end{itemize}
					\end{verbatim}
					}
				\column{.5\linewidth}
				\begin{itemize}
					\item The first item
					\item The second item
					\item The third etc \ldots
				\end{itemize}
				\end{columns}				
				\end{block}
			\begin{block}{\texttt{\textbackslash enumerate}}
			\begin{columns}
			\column{.5\linewidth}
			{\scriptsize
				\begin{verbatim}
					          \begin{enumerate}
  					        \item The first item
 					          \item The second item
 					          \item The third etc \ldots
					          \end{enumerate}
				\end{verbatim}
				}
			\column{.5\linewidth}
					\begin{enumerate}
  				\item The first item
 					 \item The second item
 					 \item The third etc \ldots
					\end{enumerate}
				\end{columns}
				\end{block}
			\begin{block}{\texttt{\textbackslash description}}
			\begin{columns}
			\column{.5\linewidth}
			{\scriptsize
				\begin{verbatim}
					          \begin{description}
 					          \item[First] The first item
					          \item[Second] The second item 
					          \end{description}
				\end{verbatim}
				}
			\column{.5\linewidth}
					\begin{description}
 					 \item[First] The first item
					 \item[Second] The second item
 					 \item[Third] The third etc \ldots
					\end{description}
			\end{columns}
			\end{block}
			\end{frame}
			
	\subsection{Columns}
			\begin{frame}[shrink, containsverbatim]{Columns}
			For two columns you can use \verb!\documentclass[twocolumn]{article}!\\
			\textbf{For more columns}
			\verb!\usepackage{multicol}!
			\begin{itemize}
			\item Can support up to ten columns.
			\item Implements a multicol environment, therefore, it is possible to mix the number of columns within a document.
			\item Additionally, the environment can be nested inside other environments, such as figure.
			\item Multicol outputs balanced columns, whereby the columns on the final page will of roughly equal length.
			\item Vertical rules between columns can be customized.
			\end{itemize}
			\begin{columns}
			\column{.5\textwidth}
			\begin{block}{Code}
			{\tiny
			\begin{verbatim}
			\begin{multicols}{4}
			text text text text\\
			text text text text\\ 
			text text text text\\ 
			text text text text\\ 
			\ldots
			\end{multicols}
			\end{verbatim}
			}
			\end{block}
			\column{.5\textwidth}
			\begin{block}{Result}
			{\scriptsize
			\begin{multicols}{4}
			text text text text\\
			text text text text\\ 
			text text text text\\ 
			text text text text\\ 
			\ldots
			\end{multicols}
			}
			\end{block}
			\end{columns}
			\end{frame}
			
	\subsection{Margin Notes and Text Color}
			\begin{frame}[shrink]{Margin Notes and Text Color}
			\textbf{Margin Notes}\\
			 Margin notes are useful for editing, reminders, etc.\\
			\texttt{\textbackslash marginpar\{margin text\}}  \\
			\texttt{\textbackslash reversemarginpar\{margin text\}}   puts note on opposite side\\*[.3cm]
\pause			
			\textbf{Text Color}\\
			Need \texttt{\textbackslash usepackage[usenames,dvipsnames]\{color\}}  \\
			Code: \texttt{\textbackslash textcolor\{declared-color\} \{text\} \\*[.3cm]}
\pause
			\begin{block}{Typesetting}
			\texttt{\textbackslash marginpar\{\textbackslash textcolor\{Maroon\}
			\{Your text here\}\}}
			\end{block}
			\end{frame}
			
	\subsection{Printing code}
		\begin{frame}[shrink, containsverbatim]{\textbackslash verb and \textbackslash begin\{verbatim\}}
			Printing text as-is in your \texttt{.tex} file requires you to use \verb!\verb+text+!.
			\begin{itemize}
			\item This behaviour is best suited to short strings or commands to be displayed in a paragraph
			\item \verb!+! is an example of a delimiter, you use any character to delimit except * or letters
			\item Longer sections of code should use \verb!\begin{verbatim}![\ldots]\verb!\end{verbatim}!
			\end{itemize}
			\begin{columns}
			\column{.5\textwidth}
			\begin{block}{Code}
			{\scriptsize
			\begin{verbatim}
			\verb!Hello World! is an example of short\\
			\verb+\verb!!+ usage
			\begin{verbatim}
			use 92panel,clear
			egen newid=group(id) // generates numeric id
			drop if eda<=15    //  restricting data to 16+
			drop if eda>=66 // restricting data to 65<
			\end{verbatim}
			\verb!\end{verbatim}!\\
			is a longer block of Stata code.}
			\end{block}
			\column{.5\textwidth}
			\begin{block}{Result}
			{\scriptsize
			\verb!Hello World! is an example of short\\
			\verb+\verb!!+ usage
			\begin{verbatim}
			use 92panel,clear
			egen newid=group(id) // generates numeric id
			drop if eda<=15    //  restricting data to 16+
			drop if eda>=66 // restricting data to 65<
			\end{verbatim}
			is a longer block of Stata code}
			\end{block}
			\end{columns}
			Note: Using the package \texttt{verbatim} in preamble allows you to comment out chunks of text by using \verb!\begin{comment}![\ldots]\verb!\end{comment}!
			\end{frame}
\end{document}