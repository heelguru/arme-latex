timer on 1
set more 1
cap net sj 12-4
cap net install sg97_5, replace
cap ssc install psmatch2, replace
cap ssc install carryforward,replace
set linesize 120
cd /mnt/nfs2/bmec/heg26
use bigpanel,clear
sort nid newt
local education primary secondary preparatory university
local indv age agesq tenure tenure2 married shortcon nocon micro small medium
local sectors sec1 sec2 sec4 sec5 sec6 sec7 sec8 sec9 sec10
local states st1 st2 st3 st4 st5 st6 st7 st8 st10 st11 st12 st13 st14  ///
st15 st16 st17 st18 st19 st20 st21 st22 st23 st24 st25 st26 st27 st28 ///
st29 st30 st31 st32
local changes condown conup cowup cowdown ocup ocdown 
local start wagestart
local fe `states' `sectors'
local rhs `education' `indv' `fe' 
local lw lnhw10
forv i=1/13{
	**Kernel Matching
		psmatch2 joiners`i' `rhs' if post`i'==0 & inregxt==1 , kernel outcome(`lw')
		gen double krn=_weight
		gen double common=_support
		sort nid newt
		by nid: carryforward krn if period`i'==1, replace
		by nid: carryforward common if period`i'==1, replace
		reg `lw' joiners`i'##post`i' [aw=krn] if common==1, vce(robust)
		mat temp=r(table)
		mat didjoin`i'=(nullmat(didjoin`i')\temp[1,8],temp[2,8])
		xtreg `lw' postjoiners`i' post`i' [aw=krn] if common==1, fe vce(robust)
		mat temp=r(table)
		mat didjoin`i'=(nullmat(didjoin`i')\temp[1,1],temp[2,1])
		drop krn common
	**Kernel Matching w changes in job chars
		psmatch2 joiners`i' `rhs' `changes' if post`i'==0 & inregxt==1 , kernel outcome(`lw')
		gen double krn=_weight
		gen double common=_support
		sort nid newt
		by nid: carryforward krn if period`i'==1, replace
		by nid: carryforward common if period`i'==1, replace
		reg `lw' joiners`i'##post`i' [aw=krn] if common==1, vce(robust)
		mat temp=r(table)
		mat didjoin`i'=(nullmat(didjoin`i')\temp[1,8],temp[2,8])
		xtreg `lw' postjoiners`i' post`i' [aw=krn] if common==1, fe vce(robust)
		mat temp=r(table)
		mat didjoin`i'=(nullmat(didjoin`i')\temp[1,1],temp[2,1])
		drop krn common
	**Mahalanobis Matching
		psmatch2 joiners`i'  if post`i'==0 & inregxt==1 , outcome(`lw') mahal(`rhs')
		gen double mahal=_weight
		gen double commonm=_support
		sort nid newt
		by nid: carryforward mahal if period`i'==1, replace
		by nid: carryforward commonm if period`i'==1, replace
		reg `lw' joiners`i'##post`i' [aw=mahal] if commonm==1, vce(robust)
		mat temp=r(table)
		mat didjoin`i'=(nullmat(didjoin`i')\temp[1,8],temp[2,8])
		xtreg `lw' postjoiners`i' post`i' [aw=mahal] if commonm==1, fe vce(robust)
		mat temp=r(table)
		mat didjoin`i'=(nullmat(didjoin`i')\temp[1,1],temp[2,1])
		drop mahal commonm
	**Mahalanobis Matching w changes in job chars
		psmatch2 joiners`i'  if post`i'==0 & inregxt==1 , outcome(`lw') mahal(`rhs' `changes')
		gen double mahal=_weight
		gen double commonm=_support
		sort nid newt
		by nid: carryforward mahal if period`i'==1, replace
		by nid: carryforward commonm if period`i'==1, replace
		reg `lw' joiners`i'##post`i' [aw=mahal] if commonm==1, vce(robust)
		mat temp=r(table)
		mat didjoin`i'=(nullmat(didjoin`i')\temp[1,8],temp[2,8])
		xtreg `lw' postjoiners`i' post`i' [aw=mahal] if commonm==1, fe vce(robust)
		mat temp=r(table)
		mat didjoin`i'=(nullmat(didjoin`i')\temp[1,1],temp[2,1])
		drop mahal commonm
}
mat didjoinall=(didjoin1,didjoin2,didjoin3,didjoin4,didjoin5,didjoin6,didjoin7,didjoin8,didjoin9,didjoin10,didjoin11,didjoin12,didjoin13)
clear
svmat didjoinall
save didjoinocdel,replace
mat didjoinp1=(didjoin1,didjoin2,didjoin3,didjoin4,didjoin5,didjoin6)
mat didjoinp2=(didjoin7,didjoin8,didjoin9,didjoin10,didjoin11,didjoin12,didjoin13)

local bc = rowsof(didjoinp1)
local cols = colsof(didjoinp1)
mat starsp1 = J(`bc',`cols',0)
forv j=1(2)`cols' {
	forv k = 1/`bc' {
		mat starsp1[`k' ,`j'] = ///
		(abs(didjoinp1[`k' ,`j']/didjoinp1[`k' ,`j'+1]) > invttail(100000 ,0.10/2)) + ///
		(abs(didjoinp1[`k' ,`j']/didjoinp1[`k' ,`j'+1]) > invttail(100000 ,0.05/2)) + ///
		(abs(didjoinp1[`k' ,`j']/didjoinp1[`k' ,`j'+1]) > invttail(100000 ,0.01/2))
	}
	}

local bc = rowsof(didjoinp2)
local cols = colsof(didjoinp2)
mat starsp2 = J(`bc',`cols',0)
forv j=1(2)`cols' {
	forv k = 1/`bc' {
		mat starsp2[`k' ,`j'] = ///
		(abs(didjoinp2[`k' ,`j']/didjoinp2[`k' ,`j'+1]) > invttail(100000 ,0.10/2)) + ///
		(abs(didjoinp2[`k' ,`j']/didjoinp2[`k' ,`j'+1]) > invttail(100000 ,0.05/2)) + ///
		(abs(didjoinp2[`k' ,`j']/didjoinp2[`k' ,`j'+1]) > invttail(100000 ,0.01/2))
	}
	}

frmttable using didpsm/joinersp1.tex, statmat(didjoinp1) substat(1) sdec(3) annotate(starsp1) asymbol(*,**,***) tex replace ///
rtitles("DiD K" \"" \"DiD K FE" \"" \ "DiD K Job" \"" \ "DiD K Job FE" \"" \ "DiD M" \"" \ "DiD M FE" \"" \ "DiD M Job" \"" \ "DiD M Job FE" \"" ) ///
ctitles("","2005q1--q2", "2005q2--q3", "2005q3--q4", "2005q4-06q1", "2006q1--q2", "2006q2--07q2") ///
fragment plain

frmttable using didpsm/joinersp2.tex, statmat(didjoinp2) substat(1) sdec(3) annotate(starsp2) asymbol(*,**,***) tex replace ///
rtitles("DiD K" \""\ "DiD K FE" \"" \ "DiD K Job" \"" \ "DiD K Job FE" \"" \ "DiD M" \"" \ "DiD M FE" \"" \ "DiD M Job" \"" \ "DiD M Job FE" \"" ) ///
ctitles("","2007q2--08q2", "2008q2--09q1", "2009q1--10q1", "2010q1--11q1", "2011q1--12q1", "2012q1--13q1", "2013q1--14q1") ///
fragment plain
timer off 1
timer list
