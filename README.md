# README #

This Repository hosts all of the documents for the ARME LaTeX course. 

### This Repo gives all of the course slides and source code.

### Contact

If you need me you can [email me](mailto:h.gutierrez-rufrancos@sussex.ac.uk)