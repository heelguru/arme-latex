cd ~/tmp
!rm -rf tabout
mkdir tabout
cd tabout
!echo "\documentclass{article}" >> top.tex
!echo "\usepackage{booktabs}" >> top.tex
!echo "\usepackage{tabularx}" >> top.tex
!echo "\begin{document}" >> top.tex
!echo "\begin{center}" >>top.tex
!echo "\footnotesize" >>top.tex
!echo "\newcolumntype{Y}{>{\raggedleft\arraybackslash}X}" >>top.tex
!echo "\begin{tabularx} {#} {@{} l Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y@{}} \\\ \hline" >>top.tex
!echo "\hline" >> bot.tex
!echo "\addlinespace[.75ex]" >> bot.tex
!echo "\end{tabularx}" >> bot.tex
!echo "\scriptsize{\emph{Source: }#}" >> bot.tex
!echo "\normalsize" >> bot.tex
!echo "\end{center}" >> bot.tex
!echo "\end{document}" >> bot.tex
sysuse cancer, clear
la var died ”Patient died”
la def ny 0 ”No” 1 ”Yes”, modify
la val died ny
recode studytime (min/10 = 1 ”10 or less months”) ///
(11/20 = 2 ”11 to 20 months”) ///
(21/30 = 3 ”21 to 30 months”) ///
(31/max = 4 ”31 or more months”) ///
, gen(stime)
la var stime ”To died or exp. end”
tabout stime died using table1.txt, ///
cells(freq col cum) format(0 1) clab(No. Col_% Cum_%) ///
replace ///
style(tex) bt cl1(2-10) cl2(2-4 5-7 8-10) font(bold) ///
topf(top.tex) botf(bot.tex) topstr(14cm) botstr(cancer.dta)
!latexmk -pdf table1.tex
!latexmk -c
