\documentclass[10pt,compress,red]{beamer}
%\usepackage{beamerarticle}
\usepackage{pgf, soul }  %note: package 'soul' is used for highlighting
\usepackage{epstopdf}
\usepackage{dtklogos,verbatim,booktabs,multibib}
\setcounter{tocdepth}{2}
\listfiles  %check log file for file dependencies
\usepackage{graphicx, url, hyperref, amsmath, multicol, fixltx2e}
\usetheme{Berlin}  %PaloAlto or Copenhagen for short presentations, long:  JuanLesPins, or Antibes , or CambridgeUS or Berlin
\usecolortheme{beaver}
 \hypersetup{
    	 pdfnewwindow=true,      % links in new window
   	 colorlinks=true,       % false: boxed links; true: colored links
  	  linkcolor=red,          % color of internal links
  	  citecolor=green,        % color of links to bibliography
  	  filecolor=magenta,      % color of file links
  	  urlcolor=red 
    }
 \AtBeginSection{
 \begin{frame}[shrink]
 \frametitle{Contents}
 \tableofcontents[currentsection]
 \end{frame}
 }
 

\title{A Concise Course in \LaTeX\\ Part 3: Further Topics in \LaTeX}
\author{Hector Rufrancos}
\institute{University of Sussex}
\date{July 2014}


\begin{document}

\frame{\titlepage}

%\section{Table of Contents}
\begin{frame}[shrink]{Table of Contents}
    \frametitle{Contents}
\tableofcontents[currentsection]
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
	\subsection{Outline}
			\begin{frame}[shrink]{Outline}
			Last time we learnt:
				\begin{itemize}
					\item How to include different elements in files
					\begin{itemize}
					\item multiple documents, Tables, Graphics, Hyper referencing
					\end{itemize}
				\end{itemize}
			This session we will:
				\begin{itemize}
					\item Maths
						\begin{itemize}
							\item In-line maths
							\item Equations
							\item Multi-line equations
							\item Matrices, etc		
						\end{itemize}						 
					\item \BibTeX
						\begin{itemize}
						\item What is \BibTeX?
						\item Bibliography structure
						\item \texttt{natbib}
						\item Suggested editors
						\end{itemize}
					\item Beamer
				\end{itemize}
			You can download model solutions to last weeks exercise \href{http://dl.dropbox.com/u/8809858/ex3solved.zip}{here}
			\end{frame}
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Maths}
	\begin{frame}[containsverbatim]{Some Maths}
	Let's say we are writing some text and I need to include something like $1+1=2$\\ \bigskip
	You can easily do this by typing \verb!$ 1 + 1 = 2 $!\\ \smallskip
	Note that it is the same as \verb!$1+1=2$! \LaTeX{} ignores the spaces\\ \bigskip
	I can also talk about $y=\alpha+\beta_{1}age+u_{i}$ \\ \smallskip \verb!$y=\alpha+\beta_{1}age+u_{i}$!
	\end{frame}
	\begin{frame}[shrink,containsverbatim]{Example}
		\begin{columns}
			\column{.5\linewidth}
			\begin{block}{Code}
			\scriptsize{
			\begin{verbatim}
				Add $a$ squared and $b$ squared
				to get $c$ squared. Or, using
				a more mathematical approach
				\begin{equation}
   				a^2 + b^2 = c^2
 				\end{equation}
				Einstein says
 				\begin{equation}
  				E = mc^2 \label{clever}
 				\end{equation}
				He didn't say
 				\begin{equation}
 	 			1 + 1 = 3 \tag{dumb}
 				\end{equation}
				This is a reference to
				\eqref{clever}.
				\end{verbatim}
				}
			\end{block}
			\column{.5\linewidth}
			\begin{block}{Result}
			\scriptsize{
			Add $a$ squared and $b$ squared
				to get $c$ squared. Or, using
				a more mathematical approach
				\begin{equation}
   				a^2 + b^2 = c^2
 				\end{equation}
				Einstein says
 				\begin{equation}
  				E = mc^2 \label{clever}
 				\end{equation}
				He didn't say
 				\begin{equation}
 	 			1 + 1 = 3 \tag{dumb}
 				\end{equation}
				This is a reference to
				\eqref{clever}.}
			\end{block}
			\end{columns}
	\end{frame}
	
	\begin{frame}[shrink,containsverbatim]{Example 2}
		\begin{columns}
		\column{.5\linewidth}
			\begin{block}{Code}
				\scriptsize{				
				\begin{verbatim}
				Add $a$ squared and $b$ squared
				to get $c$ squared. Or, using
				a more mathematical approach
 				\begin{equation*}
   				a^2 + b^2 = c^2
 				\end{equation*}
				or you can type less for the
				same effect:
 				\[ a^2 + b^2 = c^2 \]
				\end{verbatim}
				}
			\end{block}
		\column{.5\linewidth}
			\begin{block}{Result}
			Add $a$ squared and $b$ squared
			to get $c$ squared. Or, using
			a more mathematical approach
 			\begin{equation*}
  			 a^2 + b^2 = c^2
 			\end{equation*}
			or you can type less for the
			same effect:
 			\[ a^2 + b^2 = c^2 \]
			\end{block}
		\end{columns}
	\end{frame}
	
		\begin{frame}[shrink,containsverbatim]{Example 3}
		\begin{columns}
		\column{.5\linewidth}
			\begin{block}{Code}
				\scriptsize{				
				\begin{verbatim}
				This is text style:
				$\lim_{n \to \infty}
 				\sum_{k=1}^n \frac{1}{k^2}
 				= \frac{\pi^2}{6}$.\\
				And this is display style:
 				\begin{equation}
 				\lim_{n \to \infty}
  				\sum_{k=1}^n \frac{1}{k^2}
  				= \frac{\pi^2}{6}
 				\end{equation}
				\end{verbatim}
				}
			\end{block}
		\column{.5\linewidth}
			\begin{block}{Result}
			This is text style:
			$\lim_{n \to \infty}
 			\sum_{k=1}^n \frac{1}{k^2}
 			= \frac{\pi^2}{6}$.\\
			And this is display style:
 			\begin{equation}
  			\lim_{n \to \infty}
  			\sum_{k=1}^n \frac{1}{k^2}
  			= \frac{\pi^2}{6}
 			\end{equation}
			\end{block}
		\end{columns}
	\end{frame}
	
		\begin{frame}[shrink,containsverbatim]{Example 4}
		\begin{columns}
		\column{.5\linewidth}
			\begin{block}{Code}
				\scriptsize{				
				\begin{verbatim}
				A $d_{e_{e_p}}$ mathematical
				expression  followed by a
				$h^{i^{g^h}}$ expression. As
				opposed to a smashed
				\smash{$d_{e_{e_p}}$} expression
				followed by a
				\smash{$h^{i^{g^h}}$} expression.
				\end{verbatim}
				}
			\end{block}
		\column{.5\linewidth}
			\begin{block}{Result}
			A $d_{e_{e_p}}$ mathematical
			expression  followed by a
			$h^{i^{g^h}}$ expression. As
			opposed to a smashed
			\smash{$d_{e_{e_p}}$} expression
			followed by a
			\smash{$h^{i^{g^h}}$} expression.
			\end{block}
		\end{columns}
	\end{frame}
	
	\begin{frame}[shrink,containsverbatim]{Differences between text mode and maths mode}
		\begin{enumerate}
		\item Spacing derived from maths logic not from linebreaks or spacing in \texttt{.tex} file
		\item Empty lines not allowed. One paragraph per formula
		\item Each letter is a variable, if you want text you must use \texttt{\textbackslash text\{\ldots\}}
		\end{enumerate}
		\begin{columns}
		\column{.5\linewidth}
			\begin{block}{Code}
				\begin{verbatim}
				$\forall x \in \mathbb{R}:$
 				$\qquad x^{2} \geq 0$
 				$x^{2} \geq 0\qquad$
 				$\text{for all }x\in\mathbb{R}$
				\end{verbatim}
			\end{block}
		\column{.5\linewidth}
			\begin{block}{Result}
			$ \forall x \in \mathbb{R}: $
 			$ \qquad x^{2} \geq 0 $
 			\\ \smallskip
 			$ x^{2} \geq 0\qquad $
 			$ \text{for all }x\in\mathbb{R} $
			\end{block}
		\end{columns}
	\end{frame}
	
	\subsection{Building Blocks of Maths in \LaTeX}
		\begin{frame}[shrink,containsverbatim]{The Greeks}
			\begin{table}
			\label{greeks}
			\caption{Greek Letters}
			{\scriptsize
			\begin{tabular}{l c | l c | l c | l c}
			$\alpha$ & \verb!\alpha! & $\theta$ & \verb!\theta! & $\o$ & \verb!\o! & $\upsilon$ & \verb!\upsilon! \\
			$\beta$ & \verb!\beta! & $\vartheta$ & \verb!\vartheta! & $\pi$ & \verb!\pi! & $\phi$ & \verb!\phi! \\
			$\gamma$ & \verb!\gamma! & $\iota$ & \verb!\iota! & $\varpi$ & \verb!\varpi! & $\varphi$ & \verb!\varphi! \\
			$\delta$ & \verb!\delta! & $\kappa$ &  \verb!\kappa! & $\rho$ & \verb!\rho! & $\chi$ & \verb!\chi! \\
			$\epsilon$ & \verb!\epsilon! & $\lambda$ & \verb!\lambda! & $\varrho$ & \verb!\varrho! & $\psi$ & \verb!\psi! \\
			$\varepsilon$ & \verb!\varepsilon! & $\mu$ & \verb!\mu! & $\sigma$ & \verb!\sigma! & $\omega$ & \verb!\omega! \\
			$\zeta$ & \verb!\zeta! & $\nu$ & \verb!\nu! & $\varsigma$ & \verb!\varsigma!&& \\
			$\eta$ & \verb!\eta! & $\xi$ & \verb!\xi! & $\tau$ & \verb!\tau!&& \\
			$\Gamma$ & \verb!\Gamma! & $\Lambda$ & \verb!\Lambda! & $\Sigma$ & \verb!\Sigma! & $\Psi$ & \verb!\Psi! \\
			$\Delta$ & \verb!\Delta! & $\Xi$ & \verb!\Xi! & $\Upsilon$ & \verb!\Upsilon! & $\Omega$ & \verb!\Omega! \\
			$\Theta$ & \verb!\Theta! & $\Pi$ & \verb!\Pi! & $\Phi$ & \verb!\Phi!&& \\
			\hline
			\multicolumn{8}{l}{\textbf{Note:} there is no uppercase \texttt{\textbackslash Alpha}, \texttt{\textbackslash Beta}, etc. as they are same as roman letters A, B, \ldots}			
			\end{tabular}
			}
			\end{table}
		\end{frame}
		\begin{frame}[shrink,containsverbatim]{Other Maths symbols}
			\begin{block}{Example 1}
			Code:
			\begin{verbatim}
			$p^3_{ij} \qquad m_\text{hello} \qquad 
			a^x+y \neq a^{x+y} \qquad
			e^{x^2} \neq {e^x}^2$
			\end{verbatim}
			Result:\\ \smallskip
			$p^3_{ij} \qquad m_\text{hello} \qquad 
			a^x+y \neq a^{x+y} \qquad
			e^{x^2} \neq {e^x}^2$
			\end{block}
			\begin{block}{Example 2}
			Code:
			\begin{verbatim}
			$\sqrt{x} \leftrightarrow x^{1/2}
			\quad \sqrt[3]{2}
			\quad \sqrt{x^{2} + \sqrt{y}}
			\quad \surd[x^2 + y^2]$
			\end{verbatim}
			Result:\\ \smallskip
			$\sqrt{x} \leftrightarrow x^{1/2}
			\quad \sqrt[3]{2}
			\quad \sqrt{x^{2} + \sqrt{y}}
			\quad \surd[x^2 + y^2]$
			\end{block}
			\end{frame}
			\begin{frame}[shrink,containsverbatim]{Other Maths symbols}
			\begin{block}{Example 3}
			Code:
			\begin{verbatim}
			$f(x) = x^2 \qquad f'(x) = 2x
			\qquad f''(x) = 2$
			
			\begin{equation*}
			\lim_{x \rightarrow 0}
			\frac{\sin x}{x}=1
			\end{equation*}
			
			\begin{equation*}
			\sum_{i=1}^n \qquad
			\int_0^{\frac{\pi}{2}} \qquad
			\prod_\epsilon
			\end{equation*}
			\end{verbatim}
			Result:\\ \smallskip
			$f(x) = x^2 \qquad f'(x) = 2x
			\qquad f''(x) = 2$
			
			\begin{equation*}
			\lim_{x \rightarrow 0}
			\frac{\sin x}{x}=1
			\end{equation*}
			
			\begin{equation*}
			\sum_{i=1}^n \qquad
			\int_0^{\frac{\pi}{2}} \qquad
			\prod_\epsilon
			\end{equation*}
			\end{block}
			\end{frame}
			\begin{frame}[shrink,containsverbatim]{Other Maths Symbols}
			\begin{block}{Example 4}
			Code:
			\begin{verbatim}
			\begin{equation*}
  			\int_1^2 \ln x \mathrm{d}x
  			\qquad
  			\int_1^2 \ln x \,\mathrm{d}x
			\end{equation*}
			%or
			\newcommand{\ud}{\,\mathrm{d}}
			\begin{equation*}
 			\int_a^b f(x)\ud x
			\end{equation*}
			\end{verbatim}
			Result:\\ \smallskip
			\begin{equation*}
			\int_1^2 \ln x \mathrm{d}x
			\qquad
			\int_1^2 \ln x \,\mathrm{d}x
			\end{equation*}
			\newcommand{\ud}{\,\mathrm{d}}
			\begin{equation*}
 			\int_a^b f(x)\ud x
			\end{equation*}
			\end{block}
			\end{frame}
	\subsection{Your turn}
		\begin{frame}[containsverbatim]{Please Typeset the following}
		\newcommand{\ud}{\,\mathrm{d}}
		{\Large
		\begin{equation*}
		\lim_{a \rightarrow \infty}
		\int_{1}^{a} \frac{1}{x^2} \ud x = 1 
		\end{equation*}
		}	
		\end{frame}
		
		\begin{frame}[containsverbatim]{Answer}
		\begin{block}{Code}
		\begin{verbatim}
		\newcommand{\ud}{\,\mathrm{d}}
		\begin{equation*}
		\lim_{a \rightarrow \infty}
		\int_1^a \frac{1}{x^2} \ud = 1 
		\end{equation*}
		\end{verbatim}
		\end{block}
		\end{frame}
	\subsection{\texttt{\textbackslash begin\{multline\}}}
		\begin{frame}[containsverbatim,shrink]{Multiple line equations}
		\begin{columns}
		\column{.5\textwidth}
		\begin{block}{Code}
		 \begin{verbatim}
		 \begin{multline} 
		 a+b+c+d+e+f+g+h+i
		 \\ =j+k+l+m+n
		 \end{multline}
		 \end{verbatim}
		 \end{block}
		 \column{.5\textwidth}
		 \begin{block}{Result}
		 \begin{multline} 
		 a+b+c+d+e+f+g+h+i
		 \\ =j+k+l+m+n
		 \end{multline}
		\end{block}
		\end{columns}
		\end{frame}
	\subsection{\texttt{\textbackslash begin\{eqnarray\}}}
		\begin{frame}[containsverbatim,shrink]{Multiple Equations}
		\begin{columns}
		\column{.45\textwidth}
		\begin{block}{Code}
		\begin{verbatim}
		\begin{eqnarray}
		a & = b + c \\ 
		& = d + e
		\end{eqnarray}
		\begin{eqnarray}
		a & = & b + c \\ 
		& = & d + e + f + g + h + i 
		+ j + k + l \nonumber\\
		& & + \: m + n + o \\ 
		& = & p + q + r + s
		\end{eqnarray}
		\end{verbatim}
		\end{block}
		\column{.6\textwidth}
		\begin{block}{Result}
		\begin{eqnarray}
		a & = b + c \\ 
		& = d + e
		\end{eqnarray}
		\begin{eqnarray}
		a & = & b + c \\ 
		& = & d + e + f + g + h + i 
		+ j + k + l \nonumber\\
		& & + \: m + n + o \\ 
		& = & p + q + r + s
		\end{eqnarray}
		\end{block}
		\end{columns}
		\end{frame}
	\subsection{Arrays}
		\begin{frame}[containsverbatim,shrink]{Arrays}
		\begin{columns}
		\column{.5\textwidth}
			\begin{block}{Code}
			\begin{verbatim}
			\begin{equation*}
    			\mathbf{X} = \left(
      		\begin{array}{ccc}
        		x_1 & x_2 & \ldots \\
        		x_3 & x_4 & \ldots \\
        		\vdots & \vdots & \ddots
      		\end{array} \right)
  			\end{equation*}
			\end{verbatim}
			\end{block}
		\column{.5\textwidth}
			\begin{block}{Result}
			\begin{equation*}
    			\mathbf{X} = \left(
      		\begin{array}{ccc}
        		x_1 & x_2 & \ldots \\
        		x_3 & x_4 & \ldots \\
        		\vdots & \vdots & \ddots
      		\end{array} \right)
  			\end{equation*}
			\end{block}
		\end{columns}
		\end{frame}
	\subsection{Matrices}
		\begin{frame}[containsverbatim,shrink]{Matrices}
		\begin{columns}
		\column{.5\textwidth}
			\begin{block}{Code}
			{\scriptsize
			\begin{verbatim}
			\begin{equation*}
  			\begin{matrix}
			   1 & 2 \\
			   3 & 4 
			\end{matrix} \qquad 
			\begin{bmatrix}
    			p_{11} & p_{12} & \ldots & p_{1n} \\
    			p_{21} & p_{22} & \ldots & p_{2n} \\
    			\vdots & \vdots & \ddots & \vdots \\
    			p_{m1} & p_{m2} & \ldots & p_{mn}
  			\end{bmatrix}
			\end{equation*}
			\end{verbatim}
			}
			\end{block}
		\column{.5\textwidth}
			\begin{block}{Result}
			\begin{equation*}
  			\begin{matrix}
			1 & 2 \\
			3&4 
			\end{matrix} \qquad 
			\begin{bmatrix}
    			p_{11} & p_{12} & \ldots
    			& p_{1n} \\
    			p_{21} & p_{22} & \ldots
    			& p_{2n} \\
    			\vdots & \vdots & \ddots
    			& \vdots \\
    			p_{m1} & p_{m2} & \ldots
    			& p_{mn}
  			\end{bmatrix}
			\end{equation*}
			\end{block}
		\end{columns}
		\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\BibTeX}
	\subsection{Introduction}
	\begin{frame}{What is \BibTeX ?}
	\BibTeX{} is a compiler for references, which works with \LaTeX{} files.\\	 
	\BibTeX{} depends on the following:
	\begin{itemize}
	\item[(a)] an .aux file produced by LaTeX on an earlier run
	\item[(b)] a .bst file (the style file), which specifies the general reference-list style and specifies how to format individual entries, and which is written by a style 		designer [..] in a special-purpose language [..] 
	\item[(c)] .bib file(s) constituting a database of all reference-list entries the user might ever hope to use.
	\end{itemize}
	(a) and (b) are already made for you. So we shall focus on (c) \\
	\end{frame}

	\subsection{\texttt{\textbackslash begin\{thebibliography\}} vs. \BibTeX}
	\begin{frame}[containsverbatim,shrink]{Matrices}
		\begin{columns}
		\column{.5\textwidth}
		\begin{block}{\texttt{thebibliography}}
		{\scriptsize
			\begin{verbatim}			
				\documentclass[11pt,a4paper]{article}
				\begin{document}
				\maketitle
				\section{Introduction}
					Cicero \cite{cicero} said 
					``\ldots which of us ever undertakes 
					laborious physical exercise, except 
					to obtain some advantage from it?'' 
				\begin{thebibliography}{10}
					\bibitem{cicero} Cicero (84BCE). de Finibus 
					Bonorum et  Malorum.
				\end{thebibliography}
				\end{document}
				\end{verbatim}
		}
		\end{block}
		\column{.5\textwidth}
		\begin{block}{\BibTeX}
				\texttt{test.tex}
				{\scriptsize
				\begin{verbatim}
				\documentclass[11pt,a4paper]{article}
				\begin{document}
				\section{Introduction}
				Cicero \cite{cicero} said 
				``\ldots which of us ever undertakes
				laborious physical exercise, except
				to obtain some advantage from it?'' 
				\subsection{\ldots}
				\bibliography{biblio}
				\end{document}
				\end{verbatim}
				}
				\texttt{biblio.bib}
				{\scriptsize
				\begin{verbatim}
				@Book{cicero,
				Author ={Cicero},
				Title  ={de Finibus Bonorum et Malorum},
				Year   ={84BCE}
				}
				\end{verbatim}
				}
		\end{block}
		\end{columns}
		\end{frame}
		
	\subsection{\texttt{thebibliography}}
	\begin{frame}[shrink,containsverbatim]{The hard way}
	\begin{block}{Code}
		\begin{verbatim}
		It was shown in \cite{b1} \ldots
		\begin{thebibliography}{99}
		<...>
		\bibitem{b1} \~Nopo, Hugo, \textsl{Matching as a tool to decompose wage gaps}, The Review of Economics and Statistics, May 2008, 90(2), pp.220--229
		<...>
		\end{thebibliography}
		\end{verbatim}
	\end{block}

	\begin{block}{Result}
	It was shown in \cite{b1} \ldots
		\begin{thebibliography}{99}
		\bibitem{b1} \~Nopo, Hugo, \textsl{Matching as a tool to decompose wage gaps}, The Review of Economics and Statistics, May 2008, 90(2), pp.220--229
		\end{thebibliography}
	\end{block}
	\end{frame}
	
	\begin{frame}[shrink,containsverbatim]{The hard way II}
   	 \begin{block}{Code}
   	 		{\scriptsize
    		\begin{verbatim}
    		It was shown in \cite{b1} \ldots
    		\begin{thebibliography}{99}
    		<...>
    		\bibitem[\~Nopo, 2008]{b1} \~Nopo, Hugo, \textsl{Matching as a tool to decompose wage gaps}, The Review of Economics and Statistics, May 2008, 90(2), pp.220--229
    		<...>
    		\end{thebibliography}
    		\end{verbatim}
    		}
    	\end{block}
    	\begin{block}{Result}
    	It was shown in \cite{b1} \ldots
    		\begin{thebibliography}{99}
    		\bibitem[\~Nopo, 2008]{b1} \~Nopo, Hugo, \textsl{Matching as a tool to decompose wage gaps}, The Review of Economics and Statistics, May 2008, 90(2), pp.220--229
    		\end{thebibliography}
    	\end{block}
   	\end{frame}
   	
   	\begin{frame}{Cite style}
     	\begin{block}{Different Styles for citations}
     		{\scriptsize
        	\begin{tabular}{l c l}
        	Type & Example & Method\\
        	number-only & \ldots as shown in [34] \ldots & \texttt{\textbackslash usepackage\{cite\}}\\
        	author-date & \ldots as shown in (\~Nopo, 2008) \ldots& \texttt{\textbackslash usepackage\{natbib\}}\\
        	Short-title & as shown in \emph{\~Nopo, Matching as a Tool} \ldots & \texttt{\textbackslash usepackage \{jurabib\}}\\
        	footnotes& & \texttt{footbib}
        	\end{tabular}
        	}
     	\end{block}
	\end{frame}
	
	\subsection{\texttt{cite}}
	\begin{frame}[shrink,containsverbatim]{\texttt{cite} package}
    	\begin{verbatim}
    		\usepackage[opts]{cite}
    	\end{verbatim}
    	\begin{tabular}{l l}
        	without this package,& \verb!see \cite{wp1,book1,nopo08}!\\
        	results:& see [2,1,3]\\
        	with package & see [1-3]
    	\end{tabular}
    	\begin{block}{Options}
    		\begin{itemize}
            	\item \textbf{space, nospace} -- more or less space b/w numbers
            	\item \textbf{nocompress} -- [1,2,3] rather than [1--3]
            	\item \textbf{nosort} -- no linebreaks in citation
            	\item \textbf{superscript} -- citations as superscript. ie. see$^{1--3}$
        	\end{itemize}
    	\end{block}
	\end{frame}

	\subsection{\texttt{natbib}}
	\begin{frame}[containsverbatim,shrink]{\texttt{natbib} package}
	\verb!\usepackage[opts]{natbib}!
    	\begin{block}{Textual citation}
    	\begin{columns}
    	\column{0.5\linewidth}
        	\verb!\citet{jon90}!\\
        	\verb!\citet[ch.2]{jon90}!\\
    	\column{.5\textwidth}
        	Jones et al. (1990)\\
        	Jones et al. (1990, ch. 2)\\
    	\end{columns}
    	\end{block}
    	\begin{block}{Parenthetical citations}
    	\begin{columns}
    	\column{.5\textwidth}
    	    \verb!\citep{jon90}!\\
        	\verb!\citep[ch. 2]{jon90}!\\
        	\verb!\citep[see][]{jon90}!\\
        	\verb!\citep[see][ch.2]{jon90}!\\
    	\column{.5\textwidth}
        	(Jones et al., 1990)\\
        	(Jones et al., 1990, ch. 2)\\
        	(see Jones et al., 1990)\\
        	(see Jones et al., 1990, ch. 2)\\
    	\end{columns}
    	\end{block}
    	\begin{block}{Multiple citations}
    	\begin{columns}
        	\column{.5\textwidth}
        	\verb!\citet{jon90,jam91}!\\
        	\verb!\citep{jon90,jam91}!\\
        	\verb!\citep{jon90,jon91}!\\
        	\verb!\citep{jon90a,jon90b}!\\
    	\column{.5\textwidth}
        	Jones et al. (1990); James et al. (1991)\\
        	(Jones et al. 1990; James et al. 1991)\\
        	(Jones et al. 1990, 1991)\\
        	(Jones et al. 1990a, b)\\
    	\end{columns}
    	\end{block}
	\end{frame}
	
	\begin{frame}[shrink, containsverbatim]{\texttt{natbib} II}
    	\begin{block}{Suppresed Parentheses}
    	\begin{columns}
    	\column{.5\textwidth}
        	\verb!\citealt{jon90}!\\
        	\verb!\citealp{jon90}!\\
        	\verb!\citealp{jon90,jam91}!\\
        	\verb!\citealp[p. 32]{jon90}!\\
    	\column{.5\textwidth}
        	Jones et al. 1990\\
        	Jones et al., 1990\\
        	Jones et al.,1990; James et al., 1991\\
        	Jones et al., 1990, p.32\\
    	\end{columns}
       	\end{block}
   		\begin{block}{Partial citations}
    	\begin{columns}
    	\column{.5\textwidth}
    		\verb!\citeauthor{jon90}!\\
        	\verb!\citeyear{jon90}!\\
    	\column{.5\textwidth}
        	Jones et al.\\
        	1990\\
    	\end{columns}
    	\end{block}
    	\begin{block}{Forcing upper case}
    	\begin{columns}
      	\column{.5\textwidth}	
        	\verb!\citet{drob98}!\\
        	\verb!\Citet{drob98}!\\
    	\column{.5\textwidth}
        	della Robbia (1998)\\
        	Della Robbia (1998)\\
    	\end{columns}
    	\end{block}
    	\begin{block}{options}
    	\begin{itemize}
        	\item \textbf{round, square, curly, angle} -- parentheses \verb!(), [], {}, <>!
        	\item \textbf{colon, comma} -- separator for multiple citations
        	\end{itemize}
    	\end{block}
	\end{frame}

	\begin{frame}[shrink,containsverbatim]{\texttt{natbib} III}
	\texttt{natbib} is ideally used with \BibTeX, but \ldots
	    \begin{block}{Manual use}
    	You will need to add the necessary information to \verb!\bibitem!
    		\begin{verbatim}
        	\bibitem[Bennett et~al.(1996)]{BenFucSmo96}
          	Bennett, C , and W K Wootters,
        	\textsl{Some article},
        	Some Journal (1996)
        	\end{verbatim}
    	\end{block}
	\end{frame}

	\subsection{\BibTeX: Bibliography Magic}
	\begin{frame}[shrink,containsverbatim]{\BibTeX Magic: From Database to Bibliography}
	Using \BibTeX requires collecting all bibliographic data into (one or many) \texttt{.bib} file(s)
	  \begin{block}{mypapers.bib}
  	{\scriptsize	
  	  \begin{verbatim}
    	@Article{blanchflower90,
      	Author={Blanchflower, David and Oswald, Andrew},
      	Title={The Wage Curve},
      	Journal={Scandinavian Journal of Economics},
      	Volume= {92},
      	Number={2},
      	Pages={215--235},
      	year=1990
    	}
    	@Book{deaton97,
      	Author={Deaton, Angus},
      	Title={The Analysis of Household Surveys: A Microeconometric Approach to Development Policy},
      	Publisher={Johns Hopkins University Press for the World Bank},
      	Address={Baltimore},
      	year=1997
    	}
    	\end{verbatim}
  	}
  	\end{block}
	\end{frame}
	
	\begin{frame}[shrink,containsverbatim]{Steps}
    	\begin{enumerate}
    	\item Make Database
    	\item Select bibliography style e.g. plain, chicago, harvard, newapa, etc) \textbf{NB:} These require a \texttt{.bst} file.
    	\item In your \LaTeX document replace the \texttt{thebibliography} environment with:
    	\end{enumerate}	
	    \begin{block}{Example}
    	\begin{verbatim}
    	\bibliographystyle{plain}
    	\bibliography{mypapers} %separate .bib files with commas
    	%e.g. \bibliography{mypapers,mybooks,...}
    	\end{verbatim}
	\end{block}
	Now You have to run \texttt{pdflatex}$\rightarrow$\texttt{bibtex}$\rightarrow$\texttt{pdflatex}$\rightarrow$\texttt{pdflatex}
	\end{frame}

	\begin{frame}[shrink]{What's in a \texttt{.bib} file?}
	\scriptsize{
    	\begin{tabular}{l p{3cm} p{5cm}}
    	entry & mandatory fields & opt fields\\ \hline
    	@article & author, title, year, journal & volume, number, pages, month, note\\
    	@book & author or editorm title, publisher, year & volume or number, series, address, edition, month, note\\
    	@booklet & title & author, howpublished, address, month, year, note\\
    	@conference & author, title, booktitle, year & editor, volume or number, series, pages, address, publisher, organization\\
    	@inbook & author or editor, title, chapter or pages & volume, number, series, editon\\
    	@phdthesis & author, title, school, year & type, address, note, month\\
    	@unpublished & author, title, note & month, year\\
    	@misc & 1 of opt fields & author, title, howpublished, year, month note\\
    	@manual & title & author, organization, year, address, edition, month, note\\
    	@proceedings & title, year & editor, volume, series, etc.\\ \hline
    \multispan{3}{l}{Futher entry types: @incollection, @mastersthesis, @inproceedings, @techreport}
    	\end{tabular}
	}
	\end{frame}

	\begin{frame}[shrink,containsverbatim]{Some \BibTeX tips}
	    \begin{block}{\texttt{title} field}
    	Some bibliography styles make changes to title (i.e. uppercase $\leftrightarrow$ lowercase switches) use brackets to avoid this:\\
    	\verb! title = {The {P}aradox of {R}evolution}!
    	\end{block}
    	\begin{block}{\texttt{author} field}
    	Separate several authors using \texttt{and}\\
    	If a name has more than 2 parts use the form:\\
    	\verb!de la Cierra {y} Codorniu, Josep and von Neumann, John!
    	\end{block}	
	\end{frame}
	
	\begin{frame}[shrink,containsverbatim]{Bibliography Styles}
	Bibliography style are defined by \texttt{.bst} files
	\begin{itemize}
	\item Ex: \verb!abbrv, abbrvnat, alpha, harvard, chicago, jurabib, plain, apa, newapa,!, \ldots
	\item Many journals provide their own \texttt{.bst} files
	\item You can create your own (in terminal) using the \href{http://www.ctan.org/tex-archive/macros/latex/contrib/custom-bib}{\texttt{custom-bib}} package
	\end{itemize}
	\begin{block}{Custom-bib example}
	{\scriptsize
	\begin{verbatim}
	hector@fedora ~$ latex makebst.tex
	This is pdfTeX, Version 3.1415926-2.3-1.40.12 (TeX Live 2011) (format=latex 2012.4.21)  23 APR 2012 17:48
	<snip>
	***********************************
	* This is Make Bibliography Style *
	***********************************
	It makes up a docstrip batch job to produce
	a customized .bst file for running with BibTeX
	Do you want a description of the usage? (NO)

	\yn=n
	<snip>
	\end{verbatim}
	}
	\end{block}
	\end{frame}
	
\section{\texttt{beamer}}
	\begin{frame}[shrink]{Beamer basics}
	Congratulations, If we've made it this far then you know all the basics of \LaTeX.\\
	I shall now quickly do a crash course of \texttt{beamer}. However, for more indepth guides see: 
	    \begin{itemize}
    	\item \href{http://www.uncg.edu/cmp/reu/presentations/Charles\%20Batts\%20-\%20Beamer\%20Tutorial.pdf}{A short intro to beamer, in beamer} 
    	\item \href{http://www.math.umbc.edu/\~rouben/beamer/}{A quickstart to \texttt{beamer}}
    	\item The official \href{http://www.tex.ac.uk/CTAN/macros/latex/contrib/beamer/doc/beameruserguide.pdf}{\texttt{beamer} documentation}
    	\end{itemize}
	The good news: If you know \LaTeX, you can do \texttt{beamer}
	\end{frame}
	
	\subsection{The Basics}
	\begin{frame}[shrink,containsverbatim]{Beginning with \texttt{beamer}}
	To start a beamer presentation you begin as with all other \LaTeX{} documents
	    \begin{block}{Sample \texttt{beamer} presentation}
    	{\scriptsize
    	    \begin{verbatim}
        	\documentclass{beamer}
        	%preamble: you can select some packages here
        	\title{A Tiny Example}
        	\author{Hector Rufrancos}
        	\institute{University of Sussex}
        	\date{May 2012}
        	\begin{document}
        	\maketitle
        	\begin{frame}{First Slide}
        	 	Contents of the first slide
        	\end{frame}
        	\begin{frame}
         	\frametitle{Second Slide}
         		Contents of the second slide
        	\end{frame}
        	\end{document}
        	\end{verbatim}
    	}
    	\end{block}
	\end{frame}
	
	\begin{frame}[containsverbatim,shrink]{\texttt{beamer} as a document class}
	\texttt{beamer} options	
	    \begin{itemize}
    	\item \texttt{handout}
    	\item font size: 10pt,11pt(default),12pt
    	\item draft
    	\end{itemize}
	\texttt{beamer} automatically loads some packages such as:
	    \begin{itemize}
    	\item \texttt{amsmath}
    	\item \texttt{amsthm}
    	\item \texttt{array}
    	\item \texttt{enumerate}
    	\item \texttt{hyperref}
    	\item \texttt{xcolor}
    	\end{itemize}
	To call options for some of the packages such as \texttt{hyperref} it would be simply:\\
	\verb!\documentclass[hyperref=colorlinks]{beamer}!
	\end{frame}
	
	\begin{frame}[containsverbatim]{Preamble}
	These items are needed to create a title page.
	    \begin{itemize}
    	\item \textbackslash title[short title]\{title\}. The short title is used elsewhere.
    	\item \textbackslash subtitle\{subtitle\}
    	\item \textbackslash author[short name(s)]\{author 1 name\textbackslash inst\{1\} \textbackslash and author 2 name \textbackslash inst\{2\}\}.
    	\item \textbackslash institute\{\textbackslash inst\{1\}author 1 address \textbackslash and \textbackslash inst\{2\}author 2 address\}.
    	\item \textbackslash date\{desired date\}.
    	\end{itemize}
If no date is entered, the current date is automatically entered.
If no date is wanted, type \verb!\date{}!.
	\end{frame}
	
	
	   	\begin{frame}[shrink,containsverbatim]{The \texttt{frame} Environment}
    	\texttt{beamer} calls slides frames, this is because they are the output that has yet to be compiled into a presentation.\\
    	As with all environments it needs to be called:\\
    	\verb!\begin{frame}<overlay options>[frame options]{Frame title}!
    	\begin{block}{\texttt{frame} options}
     	 	\begin{itemize}
        	\item \texttt{allowframebreaks}
        	\item \texttt{allowdisplaybreaks}
        	\item \texttt{label=}
        	\item \texttt{containsverbatim}
        	\item \texttt{shrink}
        	\end{itemize}
     	as always make sure you \verb!\end{frame}!
     	You will probably use the \texttt{itemize} environment a lot in order to make bullet points so brush up on it!
     	\end{block}
    	\end{frame}
    	
    \subsection{Advanced Elements}	

    
   	\begin{frame}[containsverbatim]{\texttt{tableofcontents}}
   	You can include a table of contents in your presentation by making a frame:
		\begin{block}{Example}
        	\begin{verbatim}
        	\begin{frame}{Outline}
        	\tableofcontents
        	%or if you want to present the outline one by one you can type:
        	%\tableofcontents[pausesections]
        	\end{frame}
        	\end{verbatim}
 		\end{block}
   	You will need to compile the document twice (without making any changes) to get it to show up.
   	\end{frame}
		
	\begin{frame}[containsverbatim,shrink]{\texttt{pause} slides}
	 You can pause slides (i.e. generate new pdf slide) by using the \verb!\pause! command:
	 \begin{columns}
	 \column{.5\textwidth}
	 	\begin{block}{Code}
    	 	\begin{verbatim}
    	 		\begin{itemize}
    	 			\item Item 1 \pause
    	 			\item Item 1 \& 2 \pause
    	 			\item Item 1 \& 2 \& 3
    	 		\end{itemize}
    	 	\end{verbatim}
	 	\end{block}
	 	\column{.5\textwidth}
	 	\begin{block}{Result}
			\begin{itemize}
				\item Item 1 \pause
     			\item Item 1 \& 2 \pause
   	 			\item Item 1 \& 2 \& 3
   	 		\end{itemize}
		\end{block}
		\end{columns}
	\end{frame}
	
	\begin{frame}[containsverbatim,shrink]{Another way of doing the same thing}
    	\begin{verbatim}
    	\begin{itemize}[<+->]
        \item First point.
       	\item Second point.
	   	\item Third point.
        \end{itemize}
    	\end{verbatim}
	\end{frame}

	\begin{frame}[containsverbatim,shrink]{Highlighting current point}
	
	\begin{block}{Example}
	\begin{columns}
	\column{.5\textwidth}
		\begin{verbatim}
     	\begin{itemize}
    	\item<1-| alert@1> First point.
    	\item<2-| alert@2> Second point.
    	\item<3-| alert@3> Third point.
       	\end{itemize}
		\end{verbatim}
	\column{.5\textwidth}
		\begin{itemize}
    	\item<1-| alert@1> First point.
    	\item<2-| alert@2> Second point.
    	\item<3-| alert@3> Third point.
       	\end{itemize}
    \end{columns}
	\end{block}
	\end{frame}
	
	\begin{frame}[containsverbatim,shrink]{Two columns}
	\begin{block}{Code}
		\begin{verbatim}
		\begin{columns}
		\column{.5\textwidth}
		Some text for column 1
		\column{.5\textwidth}
		Some text for column 2
		\end{columns}
		\end{verbatim}
	\end{block}
	Note that you can use environments in columns (i.e. \texttt{tabular, includegraphics, etc, \ldots})
	\end{frame}
	
	\begin{frame}[containsverbatim]{\texttt{block}}
	You can have examples in your slides (as in these slides) with the \texttt{block} environment
	\begin{columns}
	\column{.5\textwidth}
		\begin{verbatim}
			\begin{block}{Title of the Block}
			Some text goes here
			\end{block}
		\end{verbatim}
	\column{.5\textwidth}
	\begin{block}{Title of the Block}
		Some text goes here
	\end{block}
	\end{columns}
	\end{frame}
	
	\begin{frame}[containsverbatim,shrink]{Themes}
	You can change the look of the slides by using themes (ie add \verb!\usetheme{name}! in the preamble). For the most part they fall outside the scope of this crash course on beamer. However, you can view all of the different themes in this \href{http://www.hartwork.org/beamer-theme-matrix/}{theme matrix}.\\
	A word of advice though:\\
    	\begin{description}
    	\item[Short Presentations] PaloAlto, Copenhagen, or the default
    	\item[Long Presentations] JuanLesPins, Antibes, CambridgeUS or Berlin(these slides)
    	\end{description}
	You can change the colour theme using \verb!\setcolortheme{name}!, you can see examples in the matrix mentioned above.
	\end{frame}
	
\section{Concluding Remarks}
	\begin{frame}{That's it!}
	By now you have learned the basics and more nitty--gritty aspects of \LaTeX. Of course there is more to learn, but you will have to do this yourself. All of the documentation is installed on your computer. But I suggest using the \href{http://www.stdout.org/~winston/latex/latexsheet-a4.pdf}{cheatsheet} to help you out.
	\begin{block}{Some other Websites that may be of use:}
		\begin{itemize}
		\item \href{http://tobi.oetiker.ch/lshort/lshort.pdf}{The Not So Short Introduction to \LaTeX}
		\item \href{http://en.wikibooks.org/wiki/LaTeX/}{\LaTeX Wikibook}
		\item \href{http://tex.stackexchange.com/}{\TeX Stackexchange (Q \& A site)}
		\item \href{www.google.com}{Your best friend, Google}
		\item \href{http://www.uncg.edu/cmp/reu/presentations/Charles\%20Batts\%20-\%20Beamer\%20Tutorial.pdf}{A beamer tutorial in beamer}
		\end{itemize}
	\end{block}
	\end{frame}
	
	\begin{frame}[shrink]{\LaTeX{} and other Applications}
			\begin{description}[<+->]
				\item[Texmaker]- free IDE for \LaTeX
					\begin{itemize}
						\item \url{http://www.xm1math.net/texmaker/}
					\end{itemize}
				\item[WibTeX]- \BibTeX manager for windows, shareware
					\begin{itemize}
					\item \url{http://wibtex.de/}
					\end{itemize}
				\item[BibDesk]- OS X \BibTeX manager (Excellent!)
					\begin{itemize}
					\item \url{http://bibdesk.sourceforge.net/}
					\end{itemize}
				\item[ScribTeX]- online, free \LaTeX editing
					\begin{itemize}
						\item \url{http://www.scribtex.com}
					\end{itemize}
				\item[LyX] - WYDSCCYG Processor
					\begin{itemize}
					\item Open .tex $\Rightarrow$ Export HTML(Word) $\Rightarrow$ Open in Word
					\item \url{http://www.lyx.org/}
					\end{itemize}
			\end{description}
		\end{frame}
		


	\subsubsection{externalapplications}
		\begin{frame}[shrink]{\LaTeX{} and other Applications}{Making Tables and Working with Data in External Software}
			\begin{description}[<+->]
				\item[OpenOffice/LibreOffice] -  macro/plugin to export to \LaTeX
					\begin{description}
						\item Calc2LaTeX \url{http://extensions.services.openoffice.org/project/Calc2LaTeX}
						\item Writer2LaTeX \url{http://extensions.services.openoffice.org/en/download/4076}
					\end{description}
				\item[Gnumeric] - Open Source spreadsheet programme that can export tables to \LaTeX
					\begin{description}
						\item Win and Linux \url{http://projects.gnome.org/gnumeric/downloads.shtml}
						\item If using OS X need to download through \href{www.macports.org}{MacPorts}
					\end{description}
				\item[AbiWord] -  Open Source Word alternative that can export to \LaTeX
					\begin{description}
						\item Win, Linux, OSX \url{http://www.abisource.com/download/}
					\end{description}
			\end{description}
		\end{frame}
		
						
		\begin{frame}[shrink]{\LaTeX{} and other Applications}{Making Tables and Working with Data in External Software}
			\begin{description}[<+->]
				\item[MS Office] - Convert tables to \LaTeX{} using plug-in macro
					\begin{description}[<+->]
						\item excel2latex from: \url{http://mirrors.ctan.org/support/excel2latex/Excel2LaTeX.xla}
						\item csv2latex from:  \url{http://ctan.org/pkg/csv2latex} (OSX)
					\end{description}
				\item[R] - Export \LaTeX{} tables directly from Data Analysis Software
					\begin{description}
						\item  \texttt{xtable}, from: \url{http://cran.r-project.org/web/packages/xtable/index.html}
					\end{description}
				\item[Stata] - Export tables and graphics to \LaTeX{} via packages: 
					\begin{description}
					\item \texttt{tabout, texdoc, dotex, texsave, listtex, sutex, est2tex, corrtex, graph2tex} use \texttt{findit} \emph{name} to get them.
					\end{description}
			\end{description}
		\end{frame}
		
	\subsection{Acknowledgements}
		\begin{frame}[containsverbatim]{Sources}
		The slides in this course are a heavily modified version of Eric A Booth and Emily Naiser's \href{https://sites.google.com/site/ericabooth/Home/-courses/latex_course}{\LaTeX short course}. In general I used examples from: \href{http://www.tex.ac.uk/CTAN/macros/latex/contrib/beamer/doc/beameruserguide.pdf}{\texttt{beamer} documentation}, \href{http://latex.berkeley.edu/}{\LaTeX Berkley Course}, \href{http://www.tug.org/pracjourn/2005-4/mertz/mertz.pdf}{beamer by example} ,\href{http://tobi.oetiker.ch/lshort/lshort.pdf}{The Not So Short Introduction to \LaTeX}, \href{http://www.math.msu.edu/~weil/FramesBeamerUS10.pdf}{The \texttt{beamer} class Slides}, and the \href{http://www.math.uni-leipzig.de/~hellmund/LaTeX/bibtex2.pdf}{\BibTeX{} tutorial}. In some cases I just lifted the examples and wording from these, or recreated them from the output pdf. I claim fair use to the reproduction of the material.\\
		
		\bigskip
		
		You can download the source text (\texttt{.tex}):
		\begin{itemize}
		\item \href{http://dl.dropbox.com/u/8809858/arme-lc.zip}{all of the slides in the course (zip)}
		\item for the exercises for the course: \href{http://dl.dropbox.com/u/8809858/ex1.tex}{1}, \href{http://dl.dropbox.com/u/8809858/ex2.tex}{2}, \href{http://dl.dropbox.com/u/8809858/ex3.tex}{3}, \href{http://dl.dropbox.com/u/8809858/ex4.tex}{4}
		\item solutions to exercises: \href{http://dl.dropbox.com/u/8809858/ex1solved.tex}{1}, \href{http://dl.dropbox.com/u/8809858/ex2solved.tex}{2}, \href{http://dl.dropbox.com/u/8809858/ex3solved.zip}{3}, \href{http://dl.dropbox.com/u/8809858/ex4solved.zip}{4}
		\end{itemize}
		\end{frame}
\end{document}
