*Stata and LaTeX Do File
************************
*ARME LaTeX Course
// --Setup--
ssc install sutex2, replace
ssc install tabout, replace
ssc install estout, replace
ssc install outreg, replace

// sutex2 Examples:
sysuse auto, clear
sutex2 price mpg
sutex2 price mpg, varlabels
sutex2 price mpg, varlab caption(Some other name) tablab(table1)
sutex2 price mpg, varlab caption(Some other name) tablab(table1) pl(htb!)
*note so far we haven't saved anything!
*to do so we need to use the saving option.
sutex2 price mpg, varlab sav(t1)

// tabout Example:
sysuse cancer, clear
la var died ”Patient died”
la def ny 0 ”No” 1 ”Yes”, modify
la val died ny
recode studytime (min/10 = 1 ”10 or less months”) ///
(11/20 = 2 ”11 to 20 months”) ///
(21/30 = 3 ”21 to 30 months”) ///
(31/max = 4 ”31 or more months”) ///
, gen(stime)
la var stime ”To died or exp. end”
tabout stime died using table1.txt, ///
cells(freq col cum) format(0 1) clab(No. Col_% Cum_%) ///
replace ///
style(tex) bt cl1(2-10) cl2(2-4 5-7 8-10) font(bold) ///
topf(top.tex) botf(bot.tex) topstr(14cm) botstr(cancer.dta)
*for more useful examples see the pdf file for tabout.

// estout Examples:

**esttab example:
*Fancy tables on screen!
sysuse auto,clear
qui reg price weight mpg
est sto e1
qui reg price weight mpg foreign
est sto e2
esttab e1 e2, ,star(* .10 ** .05 *** .01)
est clear
*see how we have to call est est sto each time?
*we can do the same with 
eststo: quietly regress price weight mpg
eststo: quietly regress price weight mpg foreign
esttab ,star(* .10 ** .05 *** .01)
eststo clear
*fewer lines==better!


**estout example:
eststo, title("Model 1"): quietly regress price weight mpg
eststo, title("Model 2"): quietly regress price weight mpg foreign
estout using estoutex.tex,cells(b(star fmt(3))se(par fmt(3))) ///
stats(r2 N, fmt(%9.3f %9.3f %9.0g) labels(R$^2$ N)) ///
label legend varlabels(_cons Constant) starlevels(* .10 ** .05 *** .01)

**estpost example:
estpost summarize price weight rep78 mpg
esttab, cells("count mean sd min max") noobs
esttab, cells("mean sd min max") nomtitle nonumber
estout using estpostex.tex, cells("mean sd min max") nomtitle nonumber

/*
One problem you may deal with is 
getting the marginal effects into 
a table (after say a probit or logit run)

You can do this in estout by using the following:
*/

logit foreign turn weight mpg
eststo logodds
estpost margins,dydx(*)
eststo mfx
esttab logodds mfx, mtitles star(* .10 ** .05 *** .01)
estout logodds mfx using exmargins.tex, cells(b(star fmt(3))se(par fmt(3))) ///
stats(r2 N, fmt(%9.3f %9.3f %9.0g) labels(R$^2$ N)) ///
label legend varlabels(_cons Constant) starlevels(* .10 ** .05 *** .01)

*for more examples please read the estout documentation...

// outreg Examples:
qui reg price weight mpg
outreg
outreg, varlabels bdec(3) title(Here is my fancy title!) ctitle("", (1))
qui reg price weight mpg

*Two columns!
outreg using outregex1.tex, varlabels bdec(3) ///
title(Here is my fancy title!) ctitle("", (1)) fragment tex
qui reg price weight mpg foreign
outreg using outregex1.tex, varlabels bdec(3) ///
ctitle("", (2)) merge replace tex fragment

*Margins
logit foreign turn weight mpg
outreg using outregmex.tex,varlables bdec(3) ///
ctitle("", log odds ratio) stats(b se)
margins, dydx(*)
outreg using outregmex.tex, varlabels bdec(3) ///
ctitle("", mfx) stats(b_dfdx se_dfdx)

*Summary Stats
mean weight foreign mpg
outreg using sumstatsoutreg.tex, ctitle(Variables, Means, Std Errors) ///
nostars nosubstat title("Summary Statistics") tex fragment

// frmttable Examples:

recode rep78 (1/3=1) (4=2) (5=3), gen(rep)
local cont price mpg length weight
local catg rep 
local over foreign
foreach var of local cont{
qui sdtest `var', by(`over')
	if `r(p)'<0.1{
	local opt uneq
	}
qui ttest `var', by(`over') `opt'
matrix stats=nullmat(stats) \ `r(mu_1)', `r(sd_1)', `r(mu_2)', `r(sd_2)', `r(t)'
}
foreach var of local catg{
levelsof `var', l(levels)
tab `var', gen(`var's)
foreach num of local levels{
ttest `var's`num', by(`over')
matrix prop=(nullmat(prop) \ `r(mu_1)', `r(sd_1)', `r(mu_2)', `r(sd_2)')
local names  `names'  "`var's`num'"
}
tab `var' `over', chi2
scalar chi2=`r(chi2)'
scalar rowprop=rowsof(prop)
scalar newrows=rowprop-1
matrix pad=J(newrows,1,.)
matrix newcolumn=chi2\pad
matrix prop=prop,newcolumn
matrix stats=stats\prop
}
frmttable, statmat(stats) tex fragment plain ///
ctitles( "", "Domestic","","Foreign","" \ "", "Mean", "SD", "Mean", "SD", "t / $\chi^{2}$ statistic") ///
multicol(1,2,2;1,4,2) rtitles("price"\ "mpg"\ "length"\ "weight"\ "Ok Repair record"\ "Good repair record"\ "Excellent repair record")